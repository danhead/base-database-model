<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter Base Database model
 *
 * A basic set of tools for interacting with a MYSQL database
 *
 * @author          Dan Leach
 * @created         07/25/2017
 * @version         1.1.1
 *
 * 
 */

class Base_db_model extends CI_Model {

    /**
     * the primary table of the model
     * @var string
     */
    var $table_name = "";

    /**
     * The primary index of the table you are working with
     * @var string
     */
    var $primary_index = "id";  

    /**
     * Force the get function to return a single result
     * @var boolean
     */
    var $return_single = FALSE;

    /**
     * An array of fields included in the table
     * Useful to determine if there is an editdate or adddate
     * @var array
     */
    var $table_fields = array();

    function __construct()
    {
        
        
 
    }

    /**
     * returns date format for the table
     * more accurate than using the constant NOW as that is set when the script is loaded
     * @return string
     */
    public function get_now()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * makes sure that we have all the data set that we need to
     * @return bool
     */
    private function validate_table_info()
    {
        if (empty($this->table_name)) {
            exit('no table name is set in the base database model');
        }

        if (!$this->db->table_exists($this->table_name) )
        {
           exit('the table '.$this->table_name.' does not exist');
        }

        return TRUE;
    }

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL, $select=NULL, $force_return_single=NULL) {
        
        $this->validate_table_info();

        $this->select($select);
        $this->join();
        $this->db->from($this->table_name);
        if ($where !== NULL) {
            if (is_array($where)) {
                foreach ($where as $field=>$value) {
                    $this->db->where($this->table_name.".".$field, $value);
                }
            } else {
                $this->db->where($this->table_name.".".$this->primary_index, $where);
            }
        }
        $result = $this->db->get()->result_array();
        if ($result) {
            if ($where == NULL) {
                return $result;
            } else if ($this->return_single_check($where, $force_return_single)) {
                return array_shift($result);
            } else {
                return $result;
            }
        } else {
            return false;
        }
    }

    public function return_single_check($where, $force=FALSE)
    {
        if ($force) {
            return TRUE;
        }
        if (!is_array($where)) {
            return TRUE;
        }
        if (!empty($this->unique_fields)) {
            foreach ($where as $field => $value) {
                if (in_array($field, $this->unique_fields)) {
                    return TRUE;
                }
            }
        }
        
        return FALSE;
    }

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
        
        $this->load_table_fields();

        if (!isset($data['adddate']) AND in_array('adddate', $this->table_fields)) {
            $data['adddate'] = $this->get_now();
        }

        if ($this->db->insert($this->table_name, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
        
        $this->load_table_fields();
        
        if (!isset($data['editdate']) AND in_array('editdate',$this->table_fields)) {
            $data['editdate'] = $this->get_now();
        }

        if (!is_array($where)) {
            $where = array($this->primary_index => $where);
        }
        $this->db->update($this->table_name, $data, $where);
        return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
        if (!is_array($where)) {
            $where = array($this->primary_index => $where);
        }
        $this->db->delete($this->table_name, $where);
        return $this->db->affected_rows();
    }

    /**
     * Updates record if exists, inserts if not
     * @param  Array  $data  Associative array field_name=>value to be updated
     * @param  array  $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return string primary index of affected row
     */
    public function insert_update(Array $data, $where = NULL)
    {
        $this->load_table_fields();

        if ($where !== NULL AND $record = $this->get($where)) {
            if (!isset($data['editdate']) AND in_array('editdate',$this->table_fields)) {
                $data['editdate'] = $this->get_now();
            }
            $this->update($data, $where);
            if (isset($record[$this->primary_index])) {
                return $record[$this->primary_index];
            }
            return TRUE;
        } else {
            if (!isset($data['adddate']) AND in_array('adddate',$this->table_fields)) {
                $data['adddate'] = $this->get_now();
            }
            return $this->insert($data);
        }
    }

    /**
     * Takes an array of data and format it for the table
     * @param array $data The data that you plan to insert/update into the table
     * @param array $ignore an array of fields that should be ignored like id, adddate, editdate
     */
    public function format_db_array($data, $ignore=array())
    {
        if (!$this->db->table_exists($this->table_name) )
        {
           return FALSE;
        }

        $default_ignore = array('id');
        $ignore = array_merge($default_ignore, $ignore);
        // get all the fields for the table
        $fields = $this->db->list_fields($this->table_name);
        $formatted_data = array();
        foreach ($fields as $field) {
            if (isset($data[$field]) AND !in_array($field, $ignore)) {
               $formatted_data[$field] = $data[$field];
            }
        }
        if (in_array('editdate', $fields)) {
            $formatted_data['editdate'] = $this->get_now();
        }
        return $formatted_data;
    }

    public function select($select=NULL)
    {
        switch ($select) {
            case 'manual':
                // do nothing because we custom set this
                break;
            default:
                $this->db->select('*');
                break;
        }
        
    }

    public function join()
    {
        // any lookup tables that are associated with this table
    }

    public function load_table_fields()
    {
        if (empty($this->table_fields)) {
            $this->table_fields = $this->db->list_fields($this->table_name);
        }
    }

    public function output_insert_array()
    {
        $this->load_table_fields();

        echo '$insert_array = array('."\n";
        foreach ($this->table_fields as $key => $value) {
            if ($value != $this->primary_index) {
                echo '  "'.$value.'" => $value[""]'.",\n";
            }
        }
        echo ");\n";
    }
}
      
