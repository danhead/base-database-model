
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once realpath(APPLICATION_DIR."/libraries/Base_db_model.php");

class Example_model extends Base_db_model {

    function __construct()
    {
        parent::__construct();

        $this->table_name    = "table_name";
        $this->primary_index = "id";
        $this->unique_fields = array("id","key");

    }

    ///////////////////////////////////////////////////////////////////////////////////
    // Overload functions here
    ///////////////////////////////////////////////////////////////////////////////////

    public function join()
    {
        $this->db->join('reference_table', 'reference_table.table_name_id = '.$this->table_name.'.id');
    }

    public function select($select=NULL)
    {
        switch ($select) {
            case 'manual':
                // do nothing because we custom set this
                break;
            default:
                $this->db->select('*');
                break;
        }
        
    }


    ///////////////////////////////////////////////////////////////////////////////////
    // Custom functions
    ///////////////////////////////////////////////////////////////////////////////////
}
